

# python driver for chapter 6 problem part I and II


import os
import numpy as np
import matplotlib.pyplot as plt

flops = []

for i in range(1,7):

# please try different options

#    # you can see some convergence on l2 norm of order 2 while the nonliear system seems not convergent yet.
#    options = [' -snes_type newtonls ', #-snes_stol 1e-10 ',
#               ' -snes_linesearch_type basic ',
#               '-da_grid_x 3 -da_grid_y 3 -da_refine %d '%i,
#               '-ksp_rtol 1e-9 -pc_type mg -pc_mg_levels 3 -pc_mg_galerkin', 
##               ' -ksp_monitor ',
#               '-mg_levels_ksp_norm_type unpreconditioned -mg_levels_ksp_chebyshev_esteig 0.5,1.2 ',
#               '-mg_levels_pc_type sor -pc_mg_type full -mms 1 ',
#               '-snes_monitor -snes_converged_reason -ksp_converged_reason ']
    
    
    
    
    
    


    ### this option works for the proper convergence rate plot with sequencing

    # Nonlinear solver always stops for "CONVERGED_SNORM_RELATIVE" or "DIVERGED_LINE_SEARCH"
    # But it does converge, since the l2 and l-infinity norm are both convergent perhaps properly.
    # I think it's for I didn't incorporate some error measure right in Petsc after doing snes_grid_sequence.
    options = [' -snes_type newtonls -snes_grid_sequence %d'%i,
               ' -da_refine 1 ',
               ' -ksp_rtol 1e-9 -pc_type mg -pc_mg_levels 3 -pc_mg_galerkin',
               ' -mg_levels_ksp_norm_type unpreconditioned -mg_levels_ksp_chebyshev_esteig 0.5,1.2 ',
               ' -mg_levels_pc_type sor -pc_mg_type full -mms 2 ',
               ' -snes_monitor -snes_converged_reason -ksp_converged_reason ']

#               ' -snes_stol 1e-10',    
#               ' -snes_linesearch_type basic ',





#    options = [' -snes_type newtonls -snes_grid_sequence %d'%i,
#               ' -da_refine 1 ',
#               ' -ksp_rtol 1e-12 -pc_type lu ',
#               ' -mms 1 ',
#               ' -snes_monitor -snes_converged_reason -ksp_converged_reason ',
#               ' -log_view :log%d.py:ascii_info_detail'%i]




#    options = [' -snes_type newtonls -snes_grid_sequence %d'%i,
#               ' -da_refine 1 ',
#               ' -ksp_rtol 1e-9 ',
#               ' -pc_type gamg -mms 2 ',
#               ' -snes_converged_reason -ksp_converged_reason']

    







    # the first SNES residual "0 SNES Function norm 4.865204926677e-01 " is exactly on the text book (page 78).
    #
#           ...we cannot even converge the first system.
#               0 SNES Function norm 4.865204926677e-01
#               Linear solve converged due to CONVERGED_RTOL iterations 3
#               ...
#
    # so the modified "FormFunctionLocalMMS1" must be right
    
#    options = [' -snes_type newtonls -snes_view ', #-snes_stol 1e-10 ',
##               ' -snes_linesearch_type basic ',
#               '-da_grid_x 17 -da_grid_y 17 -da_refine %d '%i,
##               '-ksp_rtol 1e-9 ',
#               ' -pc_type mg -pc_mg_levels 3 -pc_mg_galerkin', 
##               ' -ksp_monitor ',
#               '-mg_levels_ksp_norm_type unpreconditioned -mg_levels_ksp_chebyshev_esteig 0.5,1.2 ',
#               '-mg_levels_pc_type sor -pc_mg_type full -mms 1 ',
#               '-snes_monitor -snes_converged_reason -ksp_converged_reason ']







    # this Jacobian test shows the Jacobian is right.
    # (it aborts by design)
#    options = [' -snes_type test ', #-snes_stol 1e-10 ',
#               ' -snes_linesearch_type basic ',
#               '-da_grid_x 17 -da_grid_y 17 -da_refine %d '%i,
#               '-ksp_rtol 1e-9 -pc_type mg -pc_mg_levels 3 -pc_mg_galerkin', 
##               ' -ksp_monitor ',
#               '-mg_levels_ksp_norm_type unpreconditioned -mg_levels_ksp_chebyshev_esteig 0.5,1.2 ',
#               '-mg_levels_pc_type sor -pc_mg_type full -mms 1 ',
#               '-snes_monitor -snes_converged_reason -ksp_converged_reason ']





    
#    options = [' -snes_type newtonls -snes_grid_sequence %d '%i,
#               ' -snes_linesearch_type basic ',
#               ' -da_refine 1 -ksp_rtol 1e-9 ',
#               ' -pc_type lu -mms 1 ',
#               ' -snes_converged_reason ']
                  
    os.system('./bin/ex5'+' '.join(options))
    #perfmod     = __import__(modname)
    #times.append(perfmod.Stages['Main Stage']['SNESSolve'][0]['time'])
    #timesGAMG.append(perfmod.Stages['Main Stage']['MatSolve'][0]['time'])
    
    perfmod     = __import__('log%d'%i)
    flops.append(perfmod.LocalFlops[0])
    
    print 'This is i =',i,flops
    raw_input()




#PART II PLOTS

#pgamg = [0.00506645,0.00237794,0.00101019,0.000452265,0.000221282]
#fgamg = [579647, 2295170.0, 9385870.0, 134832000.0, 320356000.0]
#
#plu   = [0.0211843,0.0113242,0.00587841,0.00299793,0.00151426]
#flu   = [40637, 312898, 2553680.0, 21077000.0, 172871000.0]
#
#
#
#plt.loglog(fgamg,pgamg,'rs-',label='GAMG grid-sequencing')
#plt.loglog(flu  ,plu  ,'gs-',label='LU grid-sequencing')
#plt.legend(loc='best')
#plt.xlabel('flops')
#plt.ylabel('l2 error')
#plt.title('work-precision diagram')
#plt.grid('on')


#PART I PLOTS

#BEFORE SEQUENCING
#Ns = np.asarray([25,81,289,1089])

#mms 1: copy from code output
#N: 25 error l2 0.0326241 inf 0.434164
#N: 81 error l2 0.00675247 inf 0.178525
#N: 289 error l2 0.00178864 inf 0.143557
#N: 1089 error l2 0.000452668 inf 0.102732
#l2s= [0.0326241,0.00675247,0.00178864,0.000452668]
#lis= [0.434164 ,0.178525  ,0.143557  ,0.102732]

#mms 2:
#N: 25 error l2 0.00679262 inf 0.0576704
#N: 81 error l2 0.00614455 inf 0.157543
#N: 289 error l2 0.00210783 inf 0.143467
#N: 1089 error l2 0.000462022 inf 0.10312
#l2s= [0.00679262,0.00614455,0.00210783,0.000462022]
#lis= [0.0576704 ,0.157543  ,0.143467  ,0.10312]





##AFTER SEQUENCING
#Ns = np.asarray([169,625,2401,9409,37249])
#
##mms 1:
##N: 169 error l2 8.31782e-05 inf 0.0034683
##N: 625 error l2 1.12811e-05 inf 0.000873184
##N: 2401 error l2 1.51385e-06 inf 0.00023658
##N: 9409 error l2 1.97862e-07 inf 7.06996e-05
##N: 37249 error l2 2.53546e-08 inf 2.06393e-05
##l2s = [8.31782e-05,1.12811e-05,1.51385e-06,1.97862e-07,2.53546e-08]
##lis = [0.0034683  ,0.000873184,0.00023658 ,7.06996e-05,2.06393e-05]
#
##mms 2:
##N: 169 error l2 0.00401314 inf 0.236206
##N: 625 error l2 0.000180217 inf 0.0199962
##N: 2401 error l2 1.94277e-05 inf 0.00421692
##N: 9409 error l2 2.35215e-06 inf 0.00100285
##N: 37249 error l2 2.91232e-07 inf 0.000265741
#l2s = [0.00401314,0.000180217,1.94277e-05,2.35215e-06,2.91232e-07]
#lis = [0.236206,0.0199962,0.00421692,0.00100285,0.000265741]
#
#
#plt.loglog(Ns,l2s,'rs-',label='l_2')
#plt.loglog(Ns,1.0*np.power(Ns,-3./2),'r--',label='h^3 = N^{-3/2}')
#plt.loglog(Ns,lis,'gs-',label='l_inf')
#plt.loglog(Ns,7.0*np.power(Ns,-1.),'g--',label='h^2 = N^{-1}')
#
#
#plt.show()
##plt.title('SNES ex5 modified MMS x(1-x)y(1-y) with grid-sequencing')
#plt.title('SNES ex5 modified MMS sin(\pi x)sin(\pi y)')
#plt.xlabel('number of DOF N')
#plt.ylabel('simulation error e')
#plt.legend(loc = 'best')










