
import os

sizes = []
timesILU  = []
timesGAMG = []
for k in range(5):
    Nx          = 10*2**k
    sizes.append(Nx**2)

    #ILU    
    modname     ='perfILU%d'%k
    options     =[' -pc_type ilu ksp_type gmres -ksp_monitor',' -da_grid_x',str(Nx),'-da_grid_y',str(Nx),'-log_view',
                  ':%s.py:ascii_info_detail'%modname]
    os.system('./bin/ex5'+' '.join(options))
    perfmod     = __import__(modname)
    #times.append(perfmod.Stages['Main Stage']['SNESSolve'][0]['time'])
    timesILU.append(perfmod.Stages['Main Stage']['MatSolve'][0]['time'])

    modname     ='perfGAMG%d'%k
    options     =[' -pc_type gamg ksp_type gmres -ksp_monitor',' -da_grid_x',str(Nx),'-da_grid_y',str(Nx),'-log_view',
                  ':%s.py:ascii_info_detail'%modname]
    os.system('./bin/ex5'+' '.join(options))
    perfmod     = __import__(modname)
    #times.append(perfmod.Stages['Main Stage']['SNESSolve'][0]['time'])
    timesGAMG.append(perfmod.Stages['Main Stage']['MatSolve'][0]['time'])

#print zip(sizes,times)

#from pylab import legend,plot,loglog,show,title,xlabel,ylabel
import matplotlib.pyplot as plt

plt.loglog(sizes,timesILU,'rx-',linewidth=2,label = 'GMRES/ILU')
plt.loglog(sizes,timesGAMG,'kx-',linewidth=2,label = 'GMRES/GAMG')
plt.title('linear solve ex5')
plt.xlabel('Problem Size $N$')
plt.ylabel('Time (s)')
plt.legend(loc = 'best')
plt.show()

