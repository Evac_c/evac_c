

# CAAM 519 HW of implementing simple mixing method using SNESSHELL type in PETSc
# 2015.10.19

import os

sizes       = []
timesNewton = []
timesMixing = []
for k in range(5):
    Nx          = 10*2**k
    sizes.append(Nx**2)
    
    print '\n\n\n Solving size',Nx

    #calling the Newton solver
    modname     ='perfNewton%d'%k
    options     =[' -pc_type gamg ksp_type gmres -ksp_monitor ',
                  ' -da_grid_x',str(Nx),'-da_grid_y',str(Nx),
                  '-log_view',
                  ':%s.py:ascii_info_detail'%modname,' -mms 1 ',
                  ' -snes_monitor']
    os.system('./bin/ex5'+' '.join(options))
    perfmod     = __import__(modname)
    timesNewton.append(perfmod.Stages['Main Stage']['SNESSolve'][0]['time'])    

    #calling the Simple Mixing solver
    modname     ='perfMixing%d'%k
    options     =[' -pc_type gamg ksp_type gmres -ksp_monitor ',
                  ' -da_grid_x',str(Nx),'-da_grid_y',str(Nx),
                  '-log_view',
                  ':%s.py:ascii_info_detail'%modname,' -mms 1 ',
                  ' -snes_monitor',
                  ' -mixing519']     #extra option to call simple mixing
    os.system('./bin/ex5'+' '.join(options))
    perfmod     = __import__(modname)
    timesMixing.append(perfmod.Stages['Main Stage']['SNESSolve'][0]['time'])
    
    
#%%    


import matplotlib.pyplot as plt

plt.loglog(timesNewton,sizes,'rx-',linewidth=2,label = 'Newton')
plt.loglog(timesMixing,sizes,'kx-',linewidth=2,label = 'Simple Mixing')
plt.title('work precision diagram')
plt.xlabel('Time (s)')
plt.ylabel('Problem Size $N$')
plt.legend(loc = 'best')
plt.show()

